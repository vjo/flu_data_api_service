from flask import Flask, make_response, request
from flask_htpasswd import HtPasswdAuth

from werkzeug.middleware.proxy_fix import ProxyFix

from collections import OrderedDict
from time import monotonic as monotime

from urllib.parse import urlparse, parse_qs

from gevent.threadpool import ThreadPool
from requests import get as request_get

from json import dumps as json_dump

from os import environ
from sys import setswitchinterval

import logging

# Basic logging config
log_format = (
    f'[%(asctime)s] [%(process)d] [%(filename)s] '
    f'[%(levelname)s] %(message)s'
)
log_date_format = '%Y-%m-%d %H:%M:%S %z'

reject_logger = 'api_auth_proxy_reject_logger'

reject_log_file = '/var/log/flu_api_auth_proxy/reject_log'
reject_log_handler = logging.FileHandler(reject_log_file)
reject_log_formatter = logging.Formatter(fmt=log_format,
                                         datefmt=log_date_format)
reject_log_handler.setFormatter(reject_log_formatter)

reject_log = logging.getLogger(reject_logger)
reject_log.setLevel(logging.INFO)
reject_log.addHandler(reject_log_handler)
reject_log.propagate = False

# Set the thread switch interval to 25 microseconds
setswitchinterval(0.000025)

# Seconds to block a failing IP
fail_block_seconds = 300

# Number of failed attempts after which to block
max_fail_attempts = 5


class BlockedIPCache:
    def __init__(self, maxsize=2048):
        self.maxsize = maxsize
        self.cache = OrderedDict()

    def check(self, ip):
        cache = self.cache
        record = cache.get(ip)

        if record:
            (total_attempts, last_attempt_expire_time) = record
            current_time = monotime()

            if (
                    total_attempts >= max_fail_attempts and
                    last_attempt_expire_time < current_time
            ):
                cache[ip] = (total_attempts + 1,
                             current_time + fail_block_seconds)
                return True

        # Record not found, total attempts below threshold, or
        # last attempt time has passed.
        return False

    def update(self, ip):
        cache = self.cache
        current_time = monotime()
        record = cache.get(ip)

        if record:
            (total_attempts, last_attempt_expire_time) = record
            if last_attempt_expire_time < current_time:
                total_attempts = 0
            cache[ip] = (total_attempts + 1,
                         current_time + fail_block_seconds)
        else:
            if len(cache) >= self.maxsize:
                cache.popitem(False)
            cache[ip] = (1,
                         current_time + fail_block_seconds)

    def remove(self, ip):
        cache = self.cache
        record = cache.get(ip)

        if record:
            del cache[ip]


requestPoolSize = 4
requestWorkerPool = ThreadPool(requestPoolSize)


# The body of the Flu APU auth proxy authentication app
app = Flask(__name__)
app.config['FLASK_HTPASSWD_PATH'] = '/opt/flu_api_auth_proxy/credentials/htpasswd'
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

htpasswd = HtPasswdAuth(app)

banList = BlockedIPCache()


@app.route('/auth-fludata', methods=['GET'])
def check_fludata_request_access():
    client_ip = request.remote_addr

    # First, check if client is already banned.
    if banList.check(client_ip):
        reject_log.info(f'{client_ip} in penalty box.')
        return make_response('Nope.\n', 403)

    # Next, check that the right method was used.
    if request.method != 'GET':
        banList.update(client_ip)
        reject_log.info(f'{client_ip} used wrong method.')
        return make_response('Wrong method.\n', 403)

    # Next, check Basic auth.
    try:
        is_valid, user = htpasswd.authenticate()
    except Exception:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} provided wrong credentials.')
        return make_response('Rolling in the fail again, I see.\n', 401)

    if not is_valid:
        banList.update(client_ip)
        reject_log.info(f'{client_ip} provided wrong credentials.')
        return make_response('Guess again.\n', 401)

    # Next, we check that the path is as expected.
    #
    # This also must be correct, before we grant access.

    uri_params = request.headers.get('X-Origin-URI')
    original_qs = urlparse(uri_params)

    if ('/api/v1/flu_data_submission' not in original_qs.path):
        banList.update(client_ip)
        reject_log.info(f'{client_ip} used wrong path.')
        return make_response('Not even wrong.\n', 403)

    # We finally got it right.
    banList.remove(client_ip)
    return make_response('OK', 202)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
