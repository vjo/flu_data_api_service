#
# Duke Flu Vaccine Data microservice Dockerfile
#

# Pull base image.
FROM pypy:3

# Define the deployment directories
ENV DEPLOYMENT /opt/flu_api_service
ENV LOGDIR /var/log/flu_api_service

# Define user and group to use
ENV GUNICORN_USER nobody
ENV GUNICORN_GROUP nogroup

# Create the directory structure.
# The credentials subdirectory is intended to house an htaccess file.
RUN mkdir -p ${DEPLOYMENT}/credentials && \
        mkdir -p ${LOGDIR} && \
        chown -R ${GUNICORN_USER}:${GUNICORN_GROUP} ${LOGDIR}

# Add required packages
RUN apt-get update && apt-get -y install less

# Install all needed dependencies.
COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

# Populate the directory structure.
COPY flu_api_service.py ${DEPLOYMENT}

# Define ports
EXPOSE 8000

# Gunicorn tuning parameters
ENV NUM_WORKERS 4

# Define allowed IPs, with a default.
ENV ALLOWED_IPS localhost

# Define time zone, for logs
ENV TZ America/New_York

# Copy in profiler
COPY wsgi_profiler.py ${DEPLOYMENT}

# If you want to do profiling, do:
# export GUNICORN_ADDITIONAL_ARGS="-c ./wsgi_profiler.py"
env GUNICORN_ADDITIONAL_ARGS ""

# Change user, and run.
USER ${GUNICORN_USER}
WORKDIR ${DEPLOYMENT}
ENTRYPOINT gunicorn --bind=0.0.0.0:8000 --name=flu_api --worker-class=gevent --workers="${NUM_WORKERS}" --keep-alive=0 --forwarded-allow-ips="${ALLOWED_IPS}" --error-logfile=${LOGDIR}/error_log --access-logfile=${LOGDIR}/access_log --capture-output --reuse-port ${GUNICORN_ADDITIONAL_ARGS} flu_api_service:app
