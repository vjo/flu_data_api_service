from flask import Flask, make_response, request
from flask_htpasswd import HtPasswdAuth

from werkzeug.middleware.proxy_fix import ProxyFix

from json import dumps as json_dump

from os import environ
from sys import setswitchinterval
from time import sleep

from gevent import spawn as gevent_spawn
from gevent.queue import Queue as gQueue
from gevent.threadpool import ThreadPool

from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import UUID

from sqlalchemy import create_engine as sa_create_engine
from sqlalchemy import Column, Identity, Integer, String
from sqlalchemy import DateTime, ForeignKey

from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP

from datetime import datetime, timezone
from uuid import uuid4
from ssl import create_default_context as ssl_create_context
from traceback import format_exc

import logging

# Basic logging config
log_format = (
    '[%(asctime)s] [%(process)d] [%(filename)s] '
    '[%(levelname)s] %(message)s'
)
log_date_format = '%Y-%m-%d %H:%M:%S %z'

# Set the thread switch interval to 25 microseconds
setswitchinterval(0.000025)

# Queues and worker pools for processing emails asynchronously
mail_pool_size = 4

report_mail_queue = gQueue()
report_mail_worker_pool = ThreadPool(mail_pool_size)


def report_mail_queue_processor():
    while True:
        (email, displayName, form_status, form_data) = report_mail_queue.get()

        report_mail_worker_pool.spawn(mail_report,
                                      email, displayName,
                                      form_status, form_data)
        # Under gevent, keep this loop from being too tight.
        sleep(0)


gevent_spawn(report_mail_queue_processor)

# SMTP config
smtp_server = 'smtp.duke.edu'
smtp_port = '25'
smtp_sender = 'Duke EOHW Flu Vaccination Team <eohwflu@duke.edu>'

# SMTP logging
smtp_logger = 'flu_api_smtp_logger'

smtp_log_file = '/var/log/flu_api_service/smtp_log'
smtp_log_handler = logging.FileHandler(smtp_log_file)
smtp_log_formatter = logging.Formatter(fmt=log_format, datefmt=log_date_format)
smtp_log_handler.setFormatter(smtp_log_formatter)

smtp_log = logging.getLogger(smtp_logger)
smtp_log.setLevel(logging.INFO)
smtp_log.addHandler(smtp_log_handler)
smtp_log.propagate = False

# SQLAlchemy boilerplate
Base = declarative_base()

# Database access parameters
pg_username = environ.get('PG_USERNAME')
pg_password = environ.get('PG_PASSWORD')
pg_hostname = environ.get('PG_HOSTNAME')
pg_port = environ.get('PG_PORT')
pg_db = environ.get('PG_DATABASE')

pg_connection_string_fmt = (
    'postgresql+pg8000://{username}:{password}@' +
    '{hostname}:{port}/{database}'
)
pg_url = pg_connection_string_fmt.format(
    username=pg_username, password=pg_password,
    hostname=pg_hostname, port=pg_port,
    database=pg_db
)

# Create SqlAlchemy engine for Postgres, with SSL,
# and a sessionmaker
pg_ssl_context = ssl_create_context()
pg_engine = sa_create_engine(pg_url,
                             client_encoding='utf8',
                             connect_args={'ssl_context': pg_ssl_context},
                             pool_size=10,
                             max_overflow=0,
                             pool_recycle=600,
                             pool_pre_ping=True,
                             pool_use_lifo=True,
                             echo=False)
pg_Session = sessionmaker(bind=pg_engine)


# Define the tables we'll be using in Postgres to record what
# comes from Kuali Build.
class ApprovedFluVxnJSON(Base):
    __tablename__ = 'approved_flu_vaccinations_json'
    duid = Column(String(15), index=True)
    txn_id = Column(UUID(as_uuid=True), index=True, primary_key=True)
    form_status = Column(String(10))
    source_json = Column(JSONB)

    def __repr__(self):
        return "<ApprovedFluVaxJSON(txn_id='%s', source_json='%s')>" % (
                                self.txn_id, json_dump(self.source_json))


class ApprovedFluVxn(Base):
    __tablename__ = 'approved_flu_vaccinations'
    tbl_id = Column(Integer, Identity(), unique=True, primary_key=True)
    duid = Column(String(15), index=True)
    txn_id = Column(UUID(as_uuid=True),
                    ForeignKey('approved_flu_vaccinations_json.txn_id'),
                    index=True)
    txn_date = Column(DateTime)
    vxn_date = Column(DateTime)
    vxn_brand = Column(String(100))

    def __repr__(self):
        return ("<ApprovedFluVaccinations(tbl_id='%s', " +
                "duid='%s', txn_id='%s', txn_date='%s', " +
                "vxn_date='%s')>") % (
                    self.tbl_id, self.duid, self.txn_id, self.txn_date,
                    self.vxn_date)


# Set up tables, if not already present.
while True:
    try:
        Base.metadata.create_all(pg_engine)
    except IntegrityError:
        continue
    except SystemExit:
        print('System exit forced while in table setup loop! '
              'Investigate database connectivity.')
        break
    except Exception:
        print('An unexpected error occurred while trying to set up tables!')
        raise
    else:
        break

# Body of Flu vaccination data app
app = Flask(__name__)
app.config['FLASK_HTPASSWD_PATH'] = '/opt/flu_api_service/credentials/htpasswd'
app.config['FLASK_AUTH_ALL'] = True
app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

htpasswd = HtPasswdAuth(app)


def mail_report(smtp_recipient, displayName, form_status, form_data):
    rejection_reason = 'None provided.'
    reviewer_comments = 'None provided.'
    if (form_status == 'reject'):
        try:
            rejection_reason = form_data['rejection_reason']['label']
            reviewer_comments = form_data['reviewer_comments']
        except Exception:
            print('Some rejection documentation missing from the form!')
            print(f'User email is: {smtp_recipient}')
            print('Proceeding with safe default values.')

    text_body_reject = (
        f'Dear {displayName},\n\n'
        f'The documentation you have submitted for your '
        f'flu vaccination has been denied. Please see below for '
        f'further details provided by your reviewer:\n\n'
        f'Reason: {rejection_reason}\n'
        f'Comments: {reviewer_comments}\n\n'
        f'Please review the above reasoning and re-submit your '
        f'flu documentation using the following link: '
        f'https://duke.is/vxsfu\n'
        f'If you have any questions or concerns, please reach out '
        f'to eohwflu@duke.edu.\n\n'
        f'Respectfully,\n\n'
        f'Duke Employee Occupational Health and Wellness\n'
    )

    html_body_reject = (
        f'<html><head></head><body><p>'
        f'Dear {displayName},<br/><br/>'
        f'The documentation you have submitted for your '
        f'flu vaccination has been denied. Please see below for '
        f'further details provided by your reviewer:<br/><br/>'
        f'Reason: {rejection_reason}<br/>'
        f'Comments: {reviewer_comments}<br/><br/>'
        f'Please review the above reasoning and re-submit your '
        f'flu documentation using the following link: '
        f'<a href="https://duke.is/vxsfu">https://duke.is/vxsfu</a><br/>'
        f'If you have any questions or concerns, please reach out '
        f'to <a href="mailto:eohwflu@duke.edu">'
        f'eohwflu@duke.edu</a>.<br/><br/>'
        f'Respectfully,<br/><br/>'
        f'Duke Employee Occupational Health and Wellness</p>'
        f'</body></html>'
    )

    text_body_approve = (
        f'Dear {displayName},\n\n'
        f'The documentation you have submitted for your flu '
        f'vaccination has been approved. Your employee health '
        f'record will be updated in the next 24 hours.\n\n'
        f'Please note that outside documentation approved by '
        f'Employee Health will not be transmitted to your '
        f'personal health record (MyChart/MaestroCare).\n\n'
        f'Please reach out to eohwflu@duke.edu if you '
        f'have any questions.\n\n'
        f'Respectfully,\n\n'
        f'Duke Employee Occupational Health and Wellness\n'
    )

    html_body_approve = (
        f'<html><head></head><body><p>'
        f'Dear {displayName},<br/><br/>'
        f'The documentation you have submitted for your flu '
        f'vaccination has been approved. Your employee health '
        f'record will be updated in the next 24 hours.<br/><br/>'
        f'Please note that outside documentation approved by '
        f'Employee Health will not be transmitted to your '
        f'personal health record (MyChart/MaestroCare).<br/><br/>'
        f'Please reach out to '
        f'<a href="mailto:eohwflu@duke.edu">'
        f'eohwflu@duke.edu</a> if you have any questions.'
        f'<br/><br/>'
        f'Respectfully,<br/><br/>'
        f'Duke Employee Occupational Health and Wellness</p>'
        f'</body></html>'
    )

    text_body = text_body_reject
    html_body = html_body_reject
    subject = 'Flu Vaccination Documentation Rejected'
    if (form_status == 'accept'):
        text_body = text_body_approve
        html_body = html_body_approve
        subject = 'Flu Vaccination Documentation Approved'

    message = MIMEMultipart('alternative')
    message['From'] = smtp_sender
    message['To'] = smtp_recipient
    message['Subject'] = subject

    text_part = MIMEText(text_body, 'plain')
    html_part = MIMEText(html_body, 'html')

    message.attach(text_part)
    message.attach(html_part)

    text = message.as_string()

    ssl_context = ssl_create_context()
    with SMTP(smtp_server, smtp_port) as server:
        try:
            server.starttls(context=ssl_context)
            server.sendmail(smtp_sender, smtp_recipient, text)

            success_msg = f'Rejection report sent to {smtp_recipient}'
            if (form_status == 'accept'):
                success_msg = f'Approval report sent to {smtp_recipient}'
            smtp_log.info(success_msg)
        except Exception as e:
            fail_msg = (
                f'Report send failed to {smtp_recipient}'
            )
            smtp_log.warning(fail_msg)
            smtp_log.warning((f'Exception resulting in failure to send: '
                              f'{type(e)}'))


def put_pg_db_data(duid, form_status, source_json):
    retval = False

    session = None
    session_error = False
    try:
        session = pg_Session()
        if session:
            txn_id = uuid4()
            txn_date = datetime.now()

            json_record = ApprovedFluVxnJSON(duid=duid,
                                             txn_id=txn_id,
                                             form_status=form_status,
                                             source_json=source_json)
            session.add(json_record)
            session.commit()

            records_added = 0
            if (form_status == 'accept'):
                for key in source_json:
                    if key.startswith('flu_vax_date'):
                        raw_js_date = source_json[key]
                        if raw_js_date is None:
                            # We somehow got an empty submission date from
                            # Kuali; log it, ignore this record, and move on.
                            print(f'Invalid submission date seen for '
                                  f'transaction: {str(txn_id)}')
                            continue

                        vxn_date = (
                            datetime.fromtimestamp((raw_js_date / 1000.0),
                                                   tz=timezone.utc)
                            )

                        vxn_brand = None
                        vxn_brand_dict = source_json.get('flu_vaccine_type')
                        if vxn_brand_dict is not None:
                            vxn_brand = vxn_brand_dict.get('label')

                        if vxn_brand is None:
                            # We didn't get a vaccine brand from Kuali;
                            # might be from an old version of the form.
                            # Log that it's missing, and move on.
                            print(f'Brand not provided for '
                                  f'transaction: {str(txn_id)}')

                        vxn_record = ApprovedFluVxn(duid=duid,
                                                    txn_id=txn_id,
                                                    txn_date=txn_date,
                                                    vxn_date=vxn_date,
                                                    vxn_brand=vxn_brand)
                        session.add(vxn_record)
                        records_added += 1

            session.commit()
            if records_added > 0 or form_status != 'accept':
                retval = True
            else:
                print('No records were able to be added '
                      'to the vaccination table! Check JSON.')
        else:
            print('Unable to get connection to backend Postgres DB.')
    except Exception as e:
        print(f'Encountered a problem communicating '
              f'with backend Postgres DB: '
              f'{str(e)}')
        print('Exception backtrace follows:')
        print(format_exc())
        session_error = True

    if session_error:
        try:
            # Do our best to revert the transaction,
            # in the face of an error.
            session.rollback()
        except Exception as e:
            print(f'Encountered a problem performing rollback '
                  f'on backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    if session:
        try:
            session.close()
        except Exception as e:
            print(f'Encountered a problem closing connection '
                  f'with backend Postgres DB: '
                  f'{str(e)}')
            print('Exception backtrace follows:')
            print(format_exc())

    return retval


@app.route('/api/v1/flu_data_submission', methods=['POST'])
def add_submitted_flu_vaccination_data():
    if request.method != 'POST':
        return make_response('Malformed request.\n', 400)

    form_data = request.json

    duid = None
    displayName = None
    email = None
    try:
        duid = form_data['meta']['submittedBy']['schoolId']
        displayName = form_data['meta']['submittedBy']['displayName']
        email = form_data['meta']['submittedBy']['email']
    except Exception:
        print('Some fields missing from JSON provided by Kuali!')

    sr_review_action = None
    try:
        sr_review_action = form_data['sr_review_action']['label']
    except Exception:
        # We don't need to handle this here.
        pass

    review_action = None
    try:
        review_action = form_data['review_action']['label']
    except Exception:
        # If this is missing, kvetch.
        print('Missing review_action field. '
              'Will throw an error to Kuali, if no sr_review_action')

    # Assign the final status we will record and use.
    form_status = sr_review_action if sr_review_action else review_action

    insert_success = None
    if duid and (form_status != 'send to senior reviewer'):
        insert_success = put_pg_db_data(duid, form_status, form_data)

    if insert_success:
        if email:
            # Queue sending the report, if an email is available.
            report_mail_queue.put((email, displayName, form_status, form_data))
        else:
            # Log that the report was not sent.
            no_email_msg = (
                f'Submitter {displayName} has no email on record; '
                f'report not sent.'
            )
            smtp_log.warning(no_email_msg)
        return make_response('Update accepted.\n', 200)

    return make_response('Malformed data.\n', 400)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
