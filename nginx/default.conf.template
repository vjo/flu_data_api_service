upstream flu_api_service {
  # fail_timeout=0 means we always retry an upstream even if it failed
  # to return a good HTTP response

  server flu_api_service:8000 fail_timeout=0;
}

server {
  listen 80;

  location /.well-known/acme-challenge {
      alias /srv/letsencrypt;

      location ~ /.well-known/acme-challenge/(.*) {
          default_type text/plain;
      }
  }

  location / {
      return 301 https://HOST$request_uri;
  }
}

server {
  listen 443 ssl default_server;

  # the domain name it will serve for
  server_name HOST; # substitute your machine's IP address or FQDN

  # If they come here using HTTP, bounce them to the correct scheme
  error_page 497 https://$server_name$request_uri;
  # Or if you're on the default port 443, then this should work too
  # error_page 497 https://;

  ssl_certificate /etc/ssl/SSL.crt;
  ssl_certificate_key /etc/ssl/SSL.key;

  charset     utf-8;

  # max upload size
  client_max_body_size 4M;   # adjust to taste

  # keep alive...
  keepalive_timeout 5;

  # proxy timeouts
  proxy_connect_timeout 5s;
  proxy_read_timeout 20s;

  # Proxy to Flu api web service
  location /api/v1/flu_data_submission {
      auth_request /auth-fludata;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto https;
      proxy_set_header Host $http_host;
      proxy_set_header Authorization "Basic $credential";
      proxy_redirect off;

      proxy_pass http://flu_api_service;
  }

  location = /auth-fludata {
      proxy_pass http://flu_api_auth_proxy:8000;

      proxy_pass_request_body off;
      proxy_set_header Content-Length "0";
      proxy_set_header X-Origin-URI $request_uri;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto https;
      proxy_set_header Host $http_host;
  }

  # Deny all else.
  location / {
      deny all;
  }
}
