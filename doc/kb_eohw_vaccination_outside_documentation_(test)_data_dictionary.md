| Custom json key | Kuali json key | Gadget | Question | Filed ID | Option label | Option key | Display logic | Placeholder | Custom output field |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| vaccine_type | P5bL0NWKwj | Radios | Vaccine Type | fnSapusb5 | 1. COVID-19<br>2.  Flu | 1. HgezpqbcR<br>2.  LP8QWPb4UNi | None | None | None |
| IntegrationFill - Duke Unique Id | AsG7L7HGLu | IntegrationFill | Duke Unique Id | 5Q8mTozvq | 1. IntegrationFill | 1. None | None | None | None |
| Boolean - Duke Unique Id - Found | AsG7L7HGLu.found | Boolean | Duke Unique Id - Found | 5Q8mTozvq.found | 1. Boolean | 1. None | None | None | None |
| Text - Duke Unique Id - Affiliation | AsG7L7HGLu.affiliation | Text | Duke Unique Id - Affiliation | 5Q8mTozvq.affiliation | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - University Compliance Monitoring Group | AsG7L7HGLu.university_compliance_monitoring_group | Text | Duke Unique Id - University Compliance Monitoring Group | 5Q8mTozvq.university_compliance_monitoring_group | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccine Recent | AsG7L7HGLu.vaccine_recent | Text | Duke Unique Id - Vaccine Recent | 5Q8mTozvq.vaccine_recent | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccine Recent Date | AsG7L7HGLu.vaccine_recent_date | Text | Duke Unique Id - Vaccine Recent Date | 5Q8mTozvq.vaccine_recent_date | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccine 2 Back | AsG7L7HGLu.vaccine_2_back | Text | Duke Unique Id - Vaccine 2 Back | 5Q8mTozvq.vaccine_2_back | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccine 2 Back Date | AsG7L7HGLu.vaccine_2_back_date | Text | Duke Unique Id - Vaccine 2 Back Date | 5Q8mTozvq.vaccine_2_back_date | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccine 3 Back | AsG7L7HGLu.vaccine_3_back | Text | Duke Unique Id - Vaccine 3 Back | 5Q8mTozvq.vaccine_3_back | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccine 3 Back Date | AsG7L7HGLu.vaccine_3_back_date | Text | Duke Unique Id - Vaccine 3 Back Date | 5Q8mTozvq.vaccine_3_back_date | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Exemption | AsG7L7HGLu.exemption | Text | Duke Unique Id - Exemption | 5Q8mTozvq.exemption | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Decline | AsG7L7HGLu.decline | Text | Duke Unique Id - Decline | 5Q8mTozvq.decline | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Exemption End Date | AsG7L7HGLu.exemption_end_date | Text | Duke Unique Id - Exemption End Date | 5Q8mTozvq.exemption_end_date | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Departmental Exception | AsG7L7HGLu.departmental_exception | Text | Duke Unique Id - Departmental Exception | 5Q8mTozvq.departmental_exception | 1. Text | 1. None | None | None | None |
| Number - Duke Unique Id - Vaccine State | AsG7L7HGLu.vaccine_state | Number | Duke Unique Id - Vaccine State | 5Q8mTozvq.vaccine_state | 1. Number | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccine Compliance | AsG7L7HGLu.vaccine_compliance | Text | Duke Unique Id - Vaccine Compliance | 5Q8mTozvq.vaccine_compliance | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Eligibility Date | AsG7L7HGLu.eligibility_date | Text | Duke Unique Id - Eligibility Date | 5Q8mTozvq.eligibility_date | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Compliance Expiring Date | AsG7L7HGLu.compliance_expiring_date | Text | Duke Unique Id - Compliance Expiring Date | 5Q8mTozvq.compliance_expiring_date | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Vaccination Status Desc | AsG7L7HGLu.vaccination_status_desc | Text | Duke Unique Id - Vaccination Status Desc | 5Q8mTozvq.vaccination_status_desc | 1. Text | 1. None | None | None | None |
| Text - Duke Unique Id - Date Loaded | AsG7L7HGLu.date_loaded | Text | Duke Unique Id - Date Loaded | 5Q8mTozvq.date_loaded | 1. Text | 1. None | None | None | None |
| RichText - Duke Unique Id - Vaccinations | AsG7L7HGLu.vaccinations | RichText | Duke Unique Id - Vaccinations | 5Q8mTozvq.vaccinations | 1. RichText | 1. None | None | None | None |
| DataLink - Covid-19 Vaccinations | AAXSnsqkvR | DataLink | Covid-19 Vaccinations | sRnpKMFn7 | 1. DataLink | 1. None | None | None | vaccinations |
| Checkboxes - Submit New Covid Vax | 3Rumn66Vba | Checkboxes | Submit New Covid Vax | xlkF_LLhw | 1. I have additional COVID vaccinations that I need to submit. | 1. Cgtln973C | None | None | None |
| CreatedBy - Submitted By | Pp0XKUrKnW | CreatedBy | Submitted By | PXkdeSiTp | 1. CreatedBy | 1. None | None | None | None |
| submitted_entered_phone | p__CL70kTN | Text | Submitted Entered Phone | TMXd2nmZH | 1. Text | 1. None | None | None | None |
| DataLink - Validated Phone Number | 7T3rNc53pW | DataLink | Validated Phone Number | JJR_WhyP1 | 1. DataLink | 1. None | None | None | national_number |
| validated_phone_number | Phvrx4mJ6a | IntegrationFill | Phone Number Validation | AeaWnBCOz | 1. IntegrationFill | 1. None | None | None | None |
| Text - Phone Number Validation - Phone Verify Error | Phvrx4mJ6a.phone_verify_error | Text | Phone Number Validation - Phone Verify Error | AeaWnBCOz.phone_verify_error | 1. Text | 1. None | None | None | None |
| Boolean - Phone Number Validation - Is Valid Number | Phvrx4mJ6a.is_valid_number | Boolean | Phone Number Validation - Is Valid Number | AeaWnBCOz.is_valid_number | 1. Boolean | 1. None | None | None | None |
| Boolean - Phone Number Validation - Is Possible Number | Phvrx4mJ6a.is_possible_number | Boolean | Phone Number Validation - Is Possible Number | AeaWnBCOz.is_possible_number | 1. Boolean | 1. None | None | None | None |
| Text - Phone Number Validation - National Number | Phvrx4mJ6a.national_number | Text | Phone Number Validation - National Number | AeaWnBCOz.national_number | 1. Text | 1. None | None | None | None |
| Text - Phone Number Validation - International | Phvrx4mJ6a.international | Text | Phone Number Validation - International | AeaWnBCOz.international | 1. Text | 1. None | None | None | None |
| Text - Phone Number Validation - E164 | Phvrx4mJ6a.E164 | Text | Phone Number Validation - E164 | AeaWnBCOz.E164 | 1. Text | 1. None | None | None | None |
| flu_vaccine_date | 7uZTemrRjJ | Date | Flu Vaccine Date | Ix9rGCoME | 1. Date | 1. None | None | None | None |
| covid_vax_date_1 | 8KlfNjlsUz | Date | Covid Vax Date 1 | CRRi_Yj_n | 1. Date | 1. None | None | None | None |
| covid_manu_1 | 1o8_1Uo50a | Dropdown | Covid Manu 1 | z9VZrmuxc | 1. Pfizer<br>2.  Moderna<br>3.  AstraZeneca<br>4.  Johnson & Johnson<br>5.  NovaVax<br>6.  CoviShield<br>7.  Clinical Trial (Unknown/Blinded)<br>8.  Sinovac<br>9.  Sinopharm | 1. pFCpCFX2g<br>2.  uVQBGOW0a<br>3.  zZ4mO777g<br>4.  Z8e6pXVWl5<br>5.  G3c1ZxCwB<br>6.  1KXI-zQva<br>7.  wQqqALnRe<br>8.  OFRkwMdUr<br>9.  VMJHCnq-2 | None | None | None |
| Radios - I Have More Covid Vaccinations Included In My Documentation That I Want To Enter. | gcK923MYev | Radios | I Have More Covid Vaccinations Included In My Documentation That I Want To Enter. | leuTNE1ZM | 1. Yes<br>2.  No | 1. FpxDUwQ1w_<br>2.  Zz-kTL1-Vl | None | None | None |
| covid_vax_date_2 | Op-mCm9sEi | Date | Covid Vax Date 2 | _xXz1_588 | 1. Date | 1. None | None | None | None |
| covid_vax_date_3 | Ufypd9Oexo | Date | Covid Vax Date 3 | RhWCtq1bU | 1. Date | 1. None | Display question if:<br>'Covid Vax Date 2' is not empty. | None | None |
| covid_vax_date_4 | TLMjuAuEio | Date | Covid Vax Date 4 | f5qF4DFKz | 1. Date | 1. None | Display question if:<br>'Covid Vax Date 3' is not empty. | None | None |
| covid_vax_date_5 | CW8i2GhOFG | Date | Covid Vax Date 5 | 7i9bDwRYtl | 1. Date | 1. None | None | None | None |
| covid_vax_manu_2 | pacU08eO1U | Dropdown | Covid Vax Manu 2 | eCvXJR9Nb | 1. Pfizer<br>2.  Moderna<br>3.  AstraZeneca<br>4.  Johnson & Johnson<br>5.  NovaVax<br>6.  CoviShield<br>7.  Clinical Trial (Unknown/Blinded)<br>8.  Sinovac<br>9.  Sinopharm | 1. pFCpCFX2g<br>2.  uVQBGOW0a<br>3.  zZ4mO777g<br>4.  Z8e6pXVWl5<br>5.  G3c1ZxCwB<br>6.  1KXI-zQva<br>7.  wQqqALnRe<br>8.  OFRkwMdUr<br>9.  VMJHCnq-2 | Display question if:<br>'Covid Vax Date 2' is not empty. | None | None |
| covid_vax_manu_3 | 3fm_-V4-uJ | Dropdown | Covid Vax Manu 3 | Y1849Imig | 1. Pfizer<br>2.  Moderna<br>3.  AstraZeneca<br>4.  Johnson & Johnson<br>5.  NovaVax<br>6.  CoviShield<br>7.  Clinical Trial (Unknown/Blinded)<br>8.  Sinovac<br>9.  Sinopharm | 1. pFCpCFX2g<br>2.  uVQBGOW0a<br>3.  zZ4mO777g<br>4.  Z8e6pXVWl5<br>5.  G3c1ZxCwB<br>6.  1KXI-zQva<br>7.  wQqqALnRe<br>8.  OFRkwMdUr<br>9.  VMJHCnq-2 | Display question if:<br>'Covid Vax Date 3' is not empty. | None | None |
| covid_vax_manu_4 | bjBFsIVkCw | Dropdown | Covid Vax Manu 4 | We6kVezmm | 1. Pfizer<br>2.  Moderna<br>3.  AstraZeneca<br>4.  Johnson & Johnson<br>5.  NovaVax<br>6.  CoviShield<br>7.  Clinical Trial (Unknown/Blinded)<br>8.  Sinovac<br>9.  Sinopharm | 1. pFCpCFX2g<br>2.  uVQBGOW0a<br>3.  zZ4mO777g<br>4.  Z8e6pXVWl5<br>5.  G3c1ZxCwB<br>6.  1KXI-zQva<br>7.  wQqqALnRe<br>8.  OFRkwMdUr<br>9.  VMJHCnq-2 | Display question if:<br>'Covid Vax Date 4' is not empty. | None | None |
| covid_vax_manu_5 | 2JfEArS0nz | Dropdown | Covid Vax Manu 5 | 2BWlClc6yb | 1. Pfizer<br>2.  Moderna<br>3.  AstraZeneca<br>4.  Johnson & Johnson<br>5.  NovaVax<br>6.  CoviShield<br>7.  Clinical Trial (Unknown/Blinded)<br>8.  Sinovac<br>9.  Sinopharm | 1. pFCpCFX2g<br>2.  uVQBGOW0a<br>3.  zZ4mO777g<br>4.  Z8e6pXVWl5<br>5.  G3c1ZxCwB<br>6.  1KXI-zQva<br>7.  wQqqALnRe<br>8.  OFRkwMdUr<br>9.  VMJHCnq-2 | Display question if:<br>'Covid Vax Date 5' is not empty. | None | None |
| vax_document_1 | 8qkQzcf_ph | FileUpload | Vax Document 1 | B2kh9j0y2 | 1. FileUpload | 1. None | None | None | None |
| vax_document_2 | n8pfEiPi_9 | FileUpload | Vax Document 2 | ccHOma_SW | 1. FileUpload | 1. None | Display question if:<br>'Covid-19' is selected from 'Vaccine Type'. | None | None |
| review_action | XppvJjnQyZ | Radios | Review Action | h5LFjEiTC | 1. accept<br>2.  send to senior reviewer<br>3.  reject | 1. OS0Aro4qF<br>2.  duqdtUccrL<br>3.  7wDs8A2gP | None | None | None |
| rejection_reason | qgHW6MKTEE | Dropdown | Rejection Reason | hpsxF-mKt | 1. Documentation incomplete<br>2.  Documentation submitted on behalf of another employee<br>3.  Duplicate submission, vaccine already on record<br>4.  Incorrect documentation submitted<br>5.  Other | 1. UwSYMfYSK3b<br>2.  -uw8CSith<br>3.  HROEAhaJX<br>4.  Co_QcCO5l8<br>5.  MuYOpkxwKf | Display question if:<br>'Reject' is selected from 'Review Action' AND 'Reject' is selected from 'Sr Review Action'. | None | None |
| comments_to_senior_reviewer | vszu4lDzr9 | Textarea | Comments To Senior Reviewer | eBvZR1iFi | 1. Textarea | 1. None | Display question if:<br>'Send To Senior Reviewer' is selected from 'Review Action'. | None | None |
| reviewer_comments | zHtLcq5Kgo | Textarea | Reviewer Comments | 5z4kijOgB | 1. Textarea | 1. None | Display question if:<br>'Reject' is selected from 'Sr Review Action' AND 'Reject' is selected from 'Review Action'. | None | None |
| sr_review_action | MlNR0kMY6_ | Radios | Sr Review Action | VExdbPg3F | 1. accept<br>2.  reject | 1. OS0Aro4qF<br>2.  7wDs8A2gP | None | None | None |
| UserTypeahead - Submitter | meta.submittedBy | UserTypeahead | Submitter | meta.submittedBy | 1. UserTypeahead | 1. None | None | None | None |
| Text - Submitter - Id | meta.submittedBy.id | Text | Submitter - Id | meta.submittedBy.id | 1. Text | 1. None | None | None | None |
| Text - Submitter - Display Name | meta.submittedBy.displayName | Text | Submitter - Display Name | meta.submittedBy.displayName | 1. Text | 1. None | None | None | None |
| Text - Submitter - School Id | meta.submittedBy.schoolId | Text | Submitter - School Id | meta.submittedBy.schoolId | 1. Text | 1. None | None | None | None |
| Email - Submitter - Email | meta.submittedBy.email | Email | Submitter - Email | meta.submittedBy.email | 1. Email | 1. None | None | None | None |
| Text - Submitter - Username | meta.submittedBy.username | Text | Submitter - Username | meta.submittedBy.username | 1. Text | 1. None | None | None | None |
| Timestamp - Submitted At | meta.submittedAt | Timestamp | Submitted At | meta.submittedAt | 1. Timestamp | 1. None | None | None | None |
| UserTypeahead - Created By | meta.createdBy | UserTypeahead | Created By | meta.createdBy | 1. UserTypeahead | 1. None | None | None | None |
| Text - Created By - Id | meta.createdBy.id | Text | Created By - Id | meta.createdBy.id | 1. Text | 1. None | None | None | None |
| Text - Created By - Display Name | meta.createdBy.displayName | Text | Created By - Display Name | meta.createdBy.displayName | 1. Text | 1. None | None | None | None |
| Text - Created By - School Id | meta.createdBy.schoolId | Text | Created By - School Id | meta.createdBy.schoolId | 1. Text | 1. None | None | None | None |
| Email - Created By - Email | meta.createdBy.email | Email | Created By - Email | meta.createdBy.email | 1. Email | 1. None | None | None | None |
| Text - Created By - Username | meta.createdBy.username | Text | Created By - Username | meta.createdBy.username | 1. Text | 1. None | None | None | None |
| Timestamp - Created At | meta.createdAt | Timestamp | Created At | meta.createdAt | 1. Timestamp | 1. None | None | None | None |
| Text - Number | meta.serialNumber | Text | Number | meta.serialNumber | 1. Text | 1. None | None | None | None |
| Url - Link To Document | meta.link | Url | Link To Document | meta.link | 1. Url | 1. None | None | None | None |
| Duration - Workflow Total Run Time | meta.workflowTotalRuntime | Duration | Workflow Total Run Time | meta.workflowTotalRuntime | 1. Duration | 1. None | None | None | None |
| WorkflowStatus - Workflow Status | meta.workflowStatus | WorkflowStatus | Workflow Status | meta.workflowStatus | 1. WorkflowStatus | 1. None | None | None | None |
| WorkflowCurrentSteps - Time On Current Step | meta.currentWorkflowSteps | WorkflowCurrentSteps | Time On Current Step | meta.currentWorkflowSteps | 1. WorkflowCurrentSteps | 1. None | None | None | None |

